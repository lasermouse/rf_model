%% select a result HDF5 file

[filename, pathname] = uigetfile({'*.h5'});
results_fname = fullfile(pathname, filename);

%% get number of traces and there name

infos = h5info(results_fname);
traces = {infos.Groups.Name};
ntraces = numel(traces);

fprintf('Found %d traces:\n', ntraces);
disp(traces');

%% load receptive field coefficients (posterior mean and covariance), amd time filters

H = cellfun(@(x) h5read(results_fname, sprintf('%s/H', x)), traces, 'un', false);
H = cat(2, H{:});

W_mean = cellfun(@(x) h5read(results_fname, sprintf('%s/W', x)), traces, 'un', false);
W_mean = cat(1, W_mean{:})';

W_cov = cellfun(@(x) h5read(results_fname, sprintf('%s/W_cov', x)), traces, 'un', false);
W_cov = cat(3, W_cov{:});

%% display some fields

% change here with your input 2D stimuli matrix dimensions
nx = 18;
ny = 32;

% trace index
idx = 1;

% reshape the selected receptive field coefficients
W_reshaped = reshape(W_mean(:, idx), nx, ny, 2);
W_max = max(abs(W_reshaped(:)));

figure();
subplot(1, 2, 1);
imagesc(W_reshaped(:, :, 1), [-W_max, W_max]);
title('ON field')
subplot(1, 2, 2);
imagesc(W_reshaped(:, :, 2), [-W_max, W_max]);
title('OFF field')

%% use marginal posterior probability to threshold maps

% Here we keep only the receptive fields coefficients which are almost
% certainly bigger than a given epsilon value.

epsilon = 0.01;  % how much dF/F0 increase is something worth looking at?
alpha = 0.95;  % degree of certainty

% adjust threshold due to arbitrary scaling of time filter
threshold = epsilon / max(H(:, idx));

% marginal posterior probability of each coefficients > epsilon
% P(coeff_i * peak time filter > epsilon | data)
post_prob = 1 - normcdf(threshold, W_mean(:, idx), sqrt(diag(W_cov(:, :, idx))));
post_prob = reshape(post_prob, nx, ny, 2);

% display thresholded values
figure();
subplot(1, 2, 1);
imagesc(post_prob(:, :, 1) > alpha, [0, 1]);
title('ON field')
subplot(1, 2, 2);
imagesc(post_prob(:, :, 2) > alpha, [0, 1]);
title('OFF field')