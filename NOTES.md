# Misc notes

- speed:

    - exploit Kronecker structure of the prior covariance
    - use black box matrix-matrix product for `XtX + C_prior_inv` solve?
      (XtX is sparse, maybe, and C_prior_inv is Kronecker)
    - avoid `XtX + C_prior_inv` solve, using VBEM with a factorised posterior for
      the coefficients (be careful with variance under estimation)
    - check other approximations (Laplace, expectation propagation...)

- sparsity:

    - smooth-DPD
    - ASD prior + ARD prior on the diagonal (sandwich matrix trick)
    - spike'n slab + GP-prior on sparsity (also use sandwich matrix trick?)
    - map estimation of multivariate Laplace distribution

- other:

    - GP on the time filter
    - smooth basis function for space
    - smooth basis function for time
    - signal dependency of the noise (Poisson-Gaussian model)
    - spurious spikes? (assymetrical noise model?)
    - multiple components (spatial map/time filter pairs)
