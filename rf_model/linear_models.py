import logging
from collections import namedtuple

import numpy as np
import scipy.stats as st
import tensorflow as tf
import matplotlib.pyplot as plt


Covariance = namedtuple('Covariance', ['value', 'inv', 'logdet'])


def ridge_cov(dims, jitter):
    """ridge regression covariance matrix"""

    # covariance parameter
    alpha = tf.nn.softplus(
        tf.get_variable(
            'alpha_unconstrained', 1, initializer=tf.constant_initializer(5.)
        )
    )
    params = {'alpha': alpha}

    # ridge covariance
    ncoeffs = np.prod(dims)
    cov_prior = tf.eye(ncoeffs) * alpha

    # inverse and log-determinant of the covariance matrix
    cov_prior_inv = tf.eye(ncoeffs) / alpha
    cov_prior_logdet = tf.log(alpha) * ncoeffs

    return Covariance(cov_prior, cov_prior_inv, cov_prior_logdet), params


def ald_cov(dims, jitter):
    """ALD covariance matrix"""
    assert len(dims) == 3 and dims[0] == 2

    # covariance parameters
    alpha = tf.nn.softplus(
        tf.get_variable(
            'alpha_unconstrained', 1, initializer=tf.constant_initializer(5.)
        )
    )
    mu_xy = tf.math.tanh(
        tf.get_variable('mu_xy', 2, initializer=tf.zeros_initializer)
    )
    theta_xy = tf.nn.softplus(
        tf.get_variable(
            'theta_xy_unconstrained', 2, initializer=tf.zeros_initializer
        )
    )
    rho = tf.math.tanh(
        tf.get_variable(
            'rho_unconstrained', 1, initializer=tf.zeros_initializer
        )
    )
    params = {'alpha': alpha, 'mu_xy': mu_xy, 'theta_xy': theta_xy, 'rho': rho}

    # grid coordinates
    coords = np.meshgrid(
        *[np.linspace(-0.5, 0.5, d) for d in dims[1:]], indexing='ij'
    )
    coords = np.reshape(np.stack(coords, axis=0), [2, -1])
    coords = tf.convert_to_tensor(coords, dtype=tf.float32)

    # ALD covariance
    off_diag_term = theta_xy[0] * theta_xy[1] * rho[0]
    cov_matrix = tf.stack([
        theta_xy[0], off_diag_term, off_diag_term, theta_xy[1]
    ])
    cov_matrix = tf.reshape(cov_matrix, [2, 2])

    coords_centered = coords - mu_xy[:, None]
    scaled_dists = tf.reduce_sum(
        coords_centered * tf.linalg.solve(cov_matrix, coords_centered), axis=0
    )

    cov_prior_diag = alpha * tf.exp(-0.5 * scaled_dists) + jitter
    cov_prior_diag = tf.concat([cov_prior_diag, cov_prior_diag], axis=0)
    cov_prior = tf.diag(cov_prior_diag)

    # inverse and log-determinant of the covariance matrix
    cov_prior_inv = tf.diag(1. / cov_prior_diag)
    cov_prior_logdet = tf.reduce_sum(tf.log(cov_prior_diag))

    return Covariance(cov_prior, cov_prior_inv, cov_prior_logdet), params


def asd_cov(dims, jitter):
    """RBF covariance matrix functioni for ASD model"""
    assert len(dims) == 3 and dims[0] == 2

    # covariance parameters
    params = {
        'alpha': tf.nn.softplus(tf.get_variable('log_alpha', 1)),
        'theta_xy': tf.nn.softplus(tf.get_variable('log_theta_xy', 1)),
        'theta_on_off': tf.nn.softplus(tf.get_variable('log_theta_on_off', 1)),
    }
    thetas = [params['theta_on_off'], params['theta_xy'], params['theta_xy']]

    # squared distance between each data point
    coords = np.meshgrid(
        *[np.linspace(-d / 2, d / 2, d) for d in dims], indexing='ij'
    )
    dists = [(cs.reshape(-1, 1) - cs.ravel())**2 for cs in coords]
    dists = tf.convert_to_tensor(np.stack(dists, axis=-1), dtype=tf.float32)

    # RBF covariance
    ncoeffs = np.prod(dims)
    scaled_dists = tf.reduce_sum(dists / tf.stack(thetas, -1)**2, axis=-1)
    jitter_cov = tf.eye(ncoeffs) * jitter
    cov_prior = params['alpha'] * tf.exp(-0.5 * scaled_dists) + jitter_cov

    # inverse and log-determinant of the covariance matrix
    L_prior = tf.cholesky(cov_prior)
    cov_prior_inv = tf.cholesky_solve(L_prior, tf.eye(ncoeffs))
    cov_prior_logdet = tf.reduce_sum(tf.log(tf.diag_part(L_prior))) * 2

    return Covariance(cov_prior, cov_prior_inv, cov_prior_logdet), params


def make_linear_model(X, nlags, dims, cov_type):
    """Linear regression model with a prior on coefficients and time filter"""

    # instantiate model covariance
    if cov_type == 'ridge':
        cov_prior, cov_prior_params = ridge_cov(dims, jitter=1e-10)
    elif cov_type == 'ald':
        cov_prior, cov_prior_params = ald_cov(dims, jitter=1e-10)
    elif cov_type == 'asd':
        cov_prior, cov_prior_params = asd_cov(dims, jitter=1e-5)
    else:
        raise ValueError('Unknown covariance type')

    # create model inputs
    X = tf.convert_to_tensor(X, dtype=tf.float32)
    y = tf.placeholder(tf.float32, int(X.shape[0]) - nlags + 1)

    # define time filter and convolve input data
    ts = tf.linspace(1., 0., nlags)
    coeffs = tf.nn.softplus(tf.get_variable(
        'log_coeffs', 2, initializer=tf.constant_initializer([1, 1])
    ))
    H = tf.exp(-coeffs[0] * ts) - tf.exp(-(coeffs[0] + coeffs[1]) * ts)

    XH = tf.nn.conv2d(tf.transpose(X)[:, np.newaxis, :, np.newaxis],
                      H[np.newaxis, :, np.newaxis, np.newaxis],
                      padding='VALID', strides=[1, 1, 1, 1])[:, 0, :, 0]

    # likelihood parameters
    offset = tf.get_variable('offset', 1)
    log_sigma = tf.get_variable('log_sigma', 1)
    sigma = tf.nn.softplus(log_sigma)

    y_centered = y - offset

    # posterior precision matrix (inverse covariance) and mean
    XtX = tf.tensordot(XH, XH, axes=[1, 1])
    Xty = tf.linalg.matvec(XH, y_centered)

    ncoeffs = int(X.shape[1])
    cov_post_inv = XtX / sigma**2 + cov_prior.inv
    L_post_inv = tf.cholesky(cov_post_inv)
    mu_post = tf.cholesky_solve(L_post_inv, Xty[:, np.newaxis] / sigma**2)
    cov_post = tf.cholesky_solve(L_post_inv, tf.eye(ncoeffs))

    # marginal likelihood, also called evidence
    yty = tf.reduce_sum(y_centered * y_centered)
    cov_post_inv_logdet = tf.reduce_sum(tf.log(tf.diag_part(L_post_inv))) * 2

    evidence = (
        - 0.5 * int(y.shape[0]) * tf.log(2 * np.pi * sigma**2)
        - 0.5 * cov_prior.logdet
        - 0.5 * cov_post_inv_logdet
        - 0.5 / sigma**2 * yty
        + 0.5 * tf.reduce_sum(
            mu_post * tf.linalg.matmul(cov_post_inv, mu_post)
        )
    )

    # quantities of interest to compute and save at the end
    y_hat = tf.reduce_sum(mu_post * XH, axis=0) + offset

    params = {
        'sigma': sigma, 'mu': offset, 'coeffs': coeffs, 'H': H,
        'W': mu_post, 'W_cov': cov_post, 'cov_prior': cov_prior.value,
        'y_hat': y_hat, 'y': y, 'evidence': evidence, **cov_prior_params
    }

    # fitting function, used for each new trace
    optimizer = ScipyOptimizerInterface(-evidence)

    def fit(trace, sess):
        scale = st.median_absolute_deviation(trace)
        sess.run(tf.global_variables_initializer())
        sess.run([
            log_sigma.assign([np.log(np.expm1(scale))]),
            offset.assign([np.median(trace)])
        ])

        feed_dict = {y: trace[nlags-1:]}
        optimizer.minimize(sess, feed_dict)

        return sess.run(params, feed_dict)

    return fit


def plot_linear_model(params, threshold=0.01, alpha=0.95):
    """display main results from model fit"""

    fig, axes = plt.subplots(1, 4, figsize=(15, 4))

    def reshape_map(x):
        return x.reshape([2, nx, ny]).transpose([1, 0, 2]).reshape([nx, -1])

    _, nx, ny = params['dims']
    W = reshape_map(params['W'])
    cov_prior = reshape_map(np.diag(params['cov_prior']))

    # threshold coefficients maps based on marginal posterior
    scale = np.sqrt(np.diag(params['W_cov']))
    peak = params['H'].max()
    W_sf = st.norm(loc=params['W'].ravel(), scale=scale).sf(threshold / peak)
    W_th = reshape_map(W_sf > alpha)

    axes[0].plot(params['H'].ravel())
    axes[0].set_title('time filter')

    vmax = np.amax(np.abs(W))
    axes[1].imshow(W, vmin=-vmax, vmax=vmax)
    axes[1].set_title('ON/OFF fields')

    axes[2].imshow(cov_prior)
    axes[2].set_title('Prior variance')

    axes[3].imshow(W_th)
    axes[3].set_title('P(coeff x filter peak > {:.2f}|data) > {:.2f}'
                      .format(threshold, alpha))

    return fig, axes


class ScipyOptimizerInterface(tf.contrib.opt.ScipyOptimizerInterface):
    """Class wrapper for ScipyOptimizerInterface to avoid numerical issues

    This class wraps the original ScipyOptimizerInterface class to avoid
    crashes due to numerical instabilities, that happen especially during line
    search and due to Cholesky decompositions.
    """

    def _minimize(self, initial_val, loss_grad_func, *args, **kwargs):
        def loss_grad_func_safe(x):
            try:
                loss, gradient = loss_grad_func(x)
            except Exception:
                logging.warning(
                    'Bad computation, loss value replaced with infinity.'
                )
                loss = np.inf
                gradient = np.zeros_like(x)
            return loss, gradient

        return super()._minimize(
            initial_val, loss_grad_func_safe, *args, **kwargs
        )
