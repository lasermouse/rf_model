#!/usr/bin/env python3

from contextlib import ExitStack
import logging
import os

import defopt
import h5py
import numpy as np
import scipy.io as io

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # NOQA disable tensorflow logs
import tensorflow as tf

from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt

from rf_model.linear_models import make_linear_model, plot_linear_model
from rf_model.sns_model import make_sns_model, plot_sns_model
from rf_model.ald_model import make_ald_model, plot_ald_model


def fit(input_filename, results_filename, *, nlags=30, subset=None,
        seed=12345, X_field='stims', y_field='ca_traces', verbose=False,
        tensorflow_verbose=False, figure_filename=None, model_type='asd'):
    """Fit an Bayesian linear regression model to Calcium fluorescence traces.

    Current model imposes an ASD (automatic smoothing determination) prior on
    the coefficients and incorporates a time filter made of a difference of 2
    exponentials. More details about the model are provided in the package
    README.md file.

    Input data should be provided as a .mat file containing:

    - `nx` and `ny`, number of lines and columns of the input stimili matrix,
    - `stims`, the input stimuli matrix as a `nx*ny` by `time` sparse matrix
      (to save space ;-)).
    - `ca_traces`, the dF/F0 time series a `nrois` by `time` matrix.

    Results file is a HDF5 file, organised as follows:

    - each trace is a different group called `/trace_<id>`, `<id>` being the
      trace index starting at 0,
    - within each group, model parameters and other quantities of interest are
      saved in separate datasets

      - sigma: likelihood noise standard deviation
      - mu: likelihood offset
      - coeffs: time filter exponentials decays
      - H: time filter
      - W: mean posterior of receptive field coefficients
      - W_cov: covariance posterior of receptive field coefficients
      - alpha: scale of ASD prior covariance
      - theta_xy: lengthscale of ASD prior covariance for spatial dimensions
      - theta_on_off: lengthscale of ASD prior covariance for ON/OFF dimension
      - y_hat: predicted trace using mean posterior
      - y: original trace

    :param str input_filename: input .mat file containing traces and predictors
    :param str results_filename: output hdf5 file to store results
    :param int nlags: number of frames for the time filter
    :param int niter: number of iterations to fit the model
    :param list[int] subset: indices, starting from 0, of traces to analyze
    :param int seed: random number generator seed
    :param str X_field: name of predictors field in input file
    :param str y_field: name of responses field in input file
    :param bool verbose: display progress information
    :param bool tensorflow_verbose: display tensorflow messages
    :param str figure_filename: save receptive field figures in a.pdf file
    :param str model_type: ridge, ald, asd or sns model

    """

    # configure logging depending on verbosity
    log_level = logging.INFO if verbose else logging.ERROR
    log_format = '%(asctime)s :: %(name)s :: %(levelname)s :: %(message)s'
    logging.basicConfig(format=log_format, level=log_level)

    tf_level = logging.INFO if tensorflow_verbose else logging.ERROR
    tf.logging.set_verbosity(tf_level)

    # load and prepare the dataset
    data = io.loadmat(input_filename, squeeze_me=True)
    X, y, nx, ny = data[X_field].T, data[y_field], data['nx'], data['ny']

    X = X.toarray()
    X = np.hstack([X > 0, X < 0]).astype(float)
    dims = [2, nx, ny]

    # fix tensorflow random seed for reproducibility
    tf.random.set_random_seed(seed)

    # tensorflow configuration to avoid crashes with cuda10/cudnn and rtx2070
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True

    # create model
    if model_type in ['ridge', 'ald', 'asd']:
        fit = make_linear_model(X, nlags, dims, model_type)
        plot = plot_linear_model
    elif model_type == 'sns':
        fit = make_sns_model(X, nlags)
        plot = plot_sns_model
    elif model_type == 'ald_advi':
        fit = make_ald_model(X, nlags, dims)
        plot = plot_ald_model
    else:
        raise ValueError('Unknown model type {}.'.format(model_type))

    # context manager to properly close files, whatever happens ;-)
    with ExitStack() as stack:

        # create the files to store results and figures (if needed)
        results_file = stack.enter_context(h5py.File(results_filename, 'w'))

        if figure_filename:
            pdf_file = stack.enter_context(PdfPages(figure_filename))

        # fit a model to each trace
        subset = range(len(y)) if subset is None else subset
        for i, trace_idx in enumerate(subset, 1):

            with tf.Session(config=config) as sess:
                params = fit(y[trace_idx], sess)
                params['dims'] = dims

            logging.info('trace %d (%d / %d)', trace_idx, i, len(subset))

            if figure_filename:
                fig, _ = plot(params)
                fig.suptitle('trace {}'.format(trace_idx), fontsize='xx-large')
                fig.tight_layout(rect=[0, 0, 1, 0.94])
                pdf_file.savefig(fig)
                plt.close(fig)

            # save results in the hdf5 file
            group = results_file.create_group('trace_{}'.format(trace_idx))
            for param, value in params.items():
                group[param] = value


def main():
    defopt.run(fit)
