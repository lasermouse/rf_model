#!/usr/bin/env python3

import logging

import numpy as np
import tensorflow as tf
from tensorflow_probability import distributions as dist
import matplotlib.pyplot as plt


def normal_factor(name, shape=(), init_loc=None, init_log_scale=None):
    if init_loc is None:
        init_loc = tf.zeros_initializer()
    if init_log_scale is None:
        init_log_scale = tf.constant_initializer(-5)

    with tf.variable_scope(name):
        loc = tf.get_variable('loc', shape, initializer=init_loc)
        log_scale = tf.get_variable('log_scale', shape,
                                    initializer=init_log_scale)

    return dist.Normal(loc, tf.exp(log_scale))


class Mixture:

    def __init__(self, pi, components):
        self.pi = pi
        self.components = components

    def log_prob(self, x):
        log_probs = [comp.log_prob(x) for comp in self.components]
        return tf.reduce_logsumexp(
            tf.log(self.pi) + tf.stack(log_probs, axis=-1), axis=-1
        )

    def post_prob(self, x):
        log_probs = [comp.log_prob(x) for comp in self.components]
        log_post = tf.log(self.pi) + tf.stack(log_probs, axis=-1)
        log_norm = tf.reduce_logsumexp(log_post, axis=-1)
        return tf.exp(log_post - log_norm[..., np.newaxis])


def make_sns_model(X, nlags, nsamples=10, p0=0.01):
    """Linear regression model with a spike and slab prior and time filter"""

    ncoeffs = X.shape[1]

    # create model inputs
    X = tf.convert_to_tensor(X, dtype=tf.float32)
    y = tf.placeholder(tf.float32, int(X.shape[0]) - nlags + 1)

    # posterior distribution approximation
    H0 = np.exp(-3 * np.linspace(1, 0, nlags))
    H0 /= H0.max()
    H_post = normal_factor('H', [nlags], tf.constant_initializer(H0))
    W_post = normal_factor('W', [ncoeffs])

    H, W = H_post.sample(nsamples), W_post.sample(nsamples)

    # prior distribution
    gamma = tf.sigmoid(tf.get_variable('log_gamma', 1))
    theta = tf.nn.softplus(tf.get_variable('log_theta', 1))
    H_past = tf.concat([tf.zeros((nsamples, 1)), gamma * H[:, :-1]], axis=-1)
    H_prior = dist.Normal(H_past, theta)

    alpha0 = tf.nn.softplus(tf.get_variable('log_alpha0', 1))
    alpha1 = tf.nn.softplus(tf.get_variable('log_alpha1', 1)) + alpha0
    pi = [1 - p0, p0]
    W_prior = Mixture(pi, [dist.Normal(0, alpha0), dist.Normal(0, alpha1)])

    # likelihood
    XW = tf.tensordot(X, W, [1, 1])
    XWH = tf.nn.depthwise_conv2d(
        XW[np.newaxis, np.newaxis, :, :],
        tf.transpose(H)[np.newaxis, :, :, np.newaxis],
        padding='VALID', strides=[1, 1, 1, 1]
    )
    XWH = tf.transpose(XWH[0, 0, :, :])

    mu = tf.get_variable('mu', 1)
    sigma = tf.nn.softplus(tf.get_variable('log_sigma', 1))
    likelihood = dist.Normal(XWH + mu, sigma).log_prob(y)

    # evidence lower bound
    post_entropy = (
        tf.reduce_sum(H_post.entropy()) + tf.reduce_sum(W_post.entropy())
    )
    prior_logprob = (
        tf.reduce_sum(H_prior.log_prob(H)) + tf.reduce_sum(W_prior.log_prob(W))
    )
    elbo = (
        tf.reduce_sum(likelihood) / nsamples
        + prior_logprob / nsamples
        + post_entropy
    )

    # posterior probability of spike'n slab
    z_post = tf.reduce_mean(W_prior.post_prob(W_post.sample(50)), axis=0)

    # parameters of interest to return at the end
    params = {
        'sigma': sigma, 'mu': mu, 'theta': theta, 'gamma': gamma,
        'alpha0': alpha0, 'alpha1': alpha1, 'H': H_post.loc, 'W': W_post.loc,
        'H_scale': H_post.scale, 'W_scale': W_post.scale,
        'z_post': z_post[:, 1]
    }

    # fitting function, used for each new trace
    optimizer = tf.train.AdamOptimizer(0.01)
    train = optimizer.minimize(-elbo)

    def fit(trace, sess, maxiter=5000, winsize=200, tol=1e-5):
        feed_dict = {y: trace[nlags-1:]}

        sess.run(tf.global_variables_initializer())
        elbos = np.zeros(maxiter)
        elbo_avg = -np.inf

        for i in range(maxiter):
            _, elbos[i] = sess.run([train, elbo], feed_dict)
            if i > 0 and i % winsize == 0:
                new_elbo_avg = np.mean(elbos[i-winsize:i])
                if np.abs(elbo_avg - new_elbo_avg) < tol * np.abs(elbo_avg):
                    logging.info('early stop at %d / %d', i, maxiter)
                    break
                elbo_avg = new_elbo_avg

        elbos = elbos[:i]

        params_values = sess.run(params, feed_dict)
        params_values['elbo'] = elbos
        return params_values

    return fit


def plot_sns_model(params):
    fig, axes = plt.subplots(2, 2, figsize=(10, 8))

    _, nx, ny = params['dims']
    W = params['W'].reshape([2, nx, ny]).transpose([1, 0, 2]).reshape([nx, -1])
    z_post = params['z_post']
    z_post = z_post.reshape([2, nx, ny]).transpose([1, 0, 2]).reshape([nx, -1])

    axes[0, 0].plot(params['H'].ravel())
    axes[0, 1].plot(params['elbo'])
    vmax = np.amax(np.abs(W))
    axes[1, 0].imshow(W, vmin=-vmax, vmax=vmax)
    axes[1, 1].imshow(np.digitize(z_post, [0.5, 0.75, 0.95, 1]),
                      vmin=0, vmax=3)

    axes[0, 0].set_title('time filter')
    axes[0, 1].set_title('ELBO')
    axes[1, 0].set_title('ON/OFF fields')
    axes[1, 1].set_title('thresholded P(slab)')

    return fig, axes
