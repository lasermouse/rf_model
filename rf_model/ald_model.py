#!/usr/bin/env python3

import logging

import numpy as np
import tensorflow as tf
from tensorflow_probability import distributions as dist
import matplotlib.pyplot as plt

from rf_model.linear_models import ald_cov
from rf_model.sns_model import normal_factor


def make_ald_model(X, nlags, dims, nsamples=10):
    """Linear regression model with an ALD prior and time filter"""

    ncoeffs = X.shape[1]

    # create model inputs
    X = tf.convert_to_tensor(X, dtype=tf.float32)
    y = tf.placeholder(tf.float32, int(X.shape[0]) - nlags + 1)

    # posterior distribution approximation
    H0 = np.exp(-3 * np.linspace(1, 0, nlags))
    H0 /= H0.max()
    H_post = normal_factor('H', [nlags], tf.constant_initializer(H0))
    W_post = normal_factor('W', [ncoeffs])

    H, W = H_post.sample(nsamples), W_post.sample(nsamples)

    # prior distribution
    gamma = tf.sigmoid(tf.get_variable('log_gamma', 1))
    theta = tf.nn.softplus(tf.get_variable('log_theta', 1))
    H_past = tf.concat([tf.zeros((nsamples, 1)), gamma * H[:, :-1]], axis=-1)
    H_prior = dist.Normal(H_past, theta)

    cov_prior, cov_prior_params = ald_cov(dims, 1e-10)
    W_prior = dist.Normal(0, tf.diag_part(cov_prior.value))

    # likelihood
    XW = tf.tensordot(X, W, [1, 1])
    XWH = tf.nn.depthwise_conv2d(
        XW[np.newaxis, np.newaxis, :, :],
        tf.transpose(H)[np.newaxis, :, :, np.newaxis],
        padding='VALID', strides=[1, 1, 1, 1]
    )
    XWH = tf.transpose(XWH[0, 0, :, :])

    mu = tf.get_variable('mu', 1)
    sigma = tf.nn.softplus(tf.get_variable('log_sigma', 1))
    likelihood = dist.Normal(XWH + mu, sigma).log_prob(y)

    # evidence lower bound
    post_entropy = (
        tf.reduce_sum(H_post.entropy()) + tf.reduce_sum(W_post.entropy())
    )
    prior_logprob = (
        tf.reduce_sum(H_prior.log_prob(H)) + tf.reduce_sum(W_prior.log_prob(W))
    )
    elbo = (
        tf.reduce_sum(likelihood) / nsamples
        + prior_logprob / nsamples
        + post_entropy
    )

    # parameters of interest to return at the end
    params = {
        'sigma': sigma, 'mu': mu, 'theta': theta, 'gamma': gamma,
        'H': H_post.loc, 'W': W_post.loc,
        'H_scale': H_post.scale, 'W_scale': W_post.scale,
        'cov_prior': cov_prior.value, **cov_prior_params
    }

    # fitting function, used for each new trace
    optimizer = tf.train.AdamOptimizer(0.01)
    train = optimizer.minimize(-elbo)

    def fit(trace, sess, maxiter=50000, winsize=2000, tol=1e-6):
        feed_dict = {y: trace[nlags-1:]}

        sess.run(tf.global_variables_initializer())
        elbos = np.zeros(maxiter)
        elbo_avg = -np.inf

        for i in range(maxiter):
            _, elbos[i] = sess.run([train, elbo], feed_dict)
            if i > 0 and i % winsize == 0:
                new_elbo_avg = np.mean(elbos[i-winsize:i])
                if np.abs(elbo_avg - new_elbo_avg) < tol * np.abs(elbo_avg):
                    logging.info('early stop at %d / %d', i, maxiter)
                    break
                elbo_avg = new_elbo_avg

        elbos = elbos[:i]

        params_values = sess.run(params, feed_dict)
        params_values['elbo'] = elbos
        return params_values

    return fit


def plot_ald_model(params):
    fig, axes = plt.subplots(2, 2, figsize=(10, 8))

    _, nx, ny = params['dims']

    def reshape_map(x):
        return x.reshape([2, nx, ny]).transpose([1, 0, 2]).reshape([nx, -1])

    W = reshape_map(params['W'])
    cov_prior = reshape_map(np.diag(params['cov_prior']))

    axes[0, 0].plot(params['H'].ravel())
    axes[0, 0].set_title('time filter')

    axes[0, 1].plot(params['elbo'])
    axes[0, 1].set_title('ELBO')

    vmax = np.amax(np.abs(W))
    axes[1, 0].imshow(W, vmin=-vmax, vmax=vmax)
    axes[1, 0].set_title('ON/OFF fields')

    axes[1, 1].imshow(cov_prior)
    axes[1, 1].set_title('Prior variance')

    return fig, axes
