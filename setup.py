#!/usr/bin/env python

from setuptools import setup, find_packages


with open("README.md", "r") as fd:
    long_description = fd.read()

setup(
    name='rf_model',
    version='0.4.1',
    description='Receptive field model for Calcium imaging data',
    author='Maxime RIO',
    author_email='maxime.rio.dev@probo.fr',
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=find_packages(),
    install_requires=[
        'setuptools>=36',
        'defopt==4.0.1',
        'h5py>=2.9.0',
        'numpy>=1.16.2',
        'scipy>=1.2.1',
        'matplotlib>=3.0.3,<3.1',
        'tensorflow-gpu==1.13.1',
        'tensorflow-probability>=0.6.0,<0.7.0',
    ],
    entry_points={'console_scripts': ['rf_fit=rf_model.rf:main']}
)
