# RF model

This package contains code to fit receptive fields from Calcium data.


## Requirements

This code relies on [Tensorflow](https://www.tensorflow.org/) and GPU computing,
so will need:

- a recent Nvidia GPU (GTX 1080 or RTX 2070),
- properly installed cuda and cudnn libraries.


## Installation

To install this package, I recommend using a virtual environment, to isolate
this code and its dependencies from the rest of your system

Using python3 (from a terminal), recommended for Linux users:
```
python3 -m venv rf_model_venv
source rf_model_venv/bin/activate
pip install git+https://bitbucket.org/lasermouse/rf_model
```

Using conda (from Anaconda Prompt), recommended for Windows users:
```
conda create -n rf_model_venv python=3.5
conda activate rf_model_venv
pip install git+https://bitbucket.org/lasermouse/rf_model
```

The code has been tested on Ubuntu 16.04, with Python 3.5, Tensorflow 1.13.1,
Cuda 10.0 and a RTX 2070.


## Getting started

Open a terminal (or an Anaconda Prompt) and activate the previously created
virtual environment using

- `source rf_model_venv/bin/activate`  if you used a regular virtual environment,
- `conda activate rf_model_venv` if you used a conda environement.

The main program is the `rf_fit` script. To know more about its options and
input/output files format, use the `--help`/`-h` option as follows:
```
rf_fit --help
```

To fit an input data file `<input_data.mat>`, call `rf_fit` as follows:
```
rf_fit <input_data.mat> output_data.h5 -f output_fig.pdf -v
```
where `output_data.h5` is the output results file and `output_fig.pdf` is the
output figures file. The `-v` flag enables more messages and figures display
during the computations.

An example input file `test_data.mat` is provided in the `examples` folder. A
Matlab script `read_results.m` is also provided to show how to read data from
a results file and display some maps.


## Model details

Current receptive field model is made of a linear regression model with a
Gaussian likelihood:
```
p(y_t|\mu,\sigma,W,H,) \sim \mathcal{N}(y_t|W^T . (X * H)_t + \mu, \sigma^2)
```
where `X * H` denotes a convolution of the time filter `H` and the input
stimuli matrix `X` over the time axis.

The time filter is expressed as a difference of exponentials:
```
H(t) = e^{-\tau_1 t} - e^{-(\tau_1 + \tau_2) t}
```
st. `\tau_1 > 0` and `\tau_2 > 0`.

In addition, an ASD prior is put on the receptive field coeffcients:
```
p(W|\theta,\gamma) \sim \mathcal{N}(W|0, C^{-1})
```
where elements of the prior covariance matrix are defined as:
```
C_{ij} = \gamma\ \Pi_d\,e^{-(W_{i,d} - W_{j,d})^2 / \theta_d}
```
and `d` indexes the spatial and ON/OFF dimensions.

Parameters of the model are fitted using an empirical Bayes method, marginal
likelihood maximization. Scipy's L-BFGS-B optimizer is used to maximize the
marginal likelihood of the model (known in closed form) wrt. parameters, the
gradients being provided by Tensorflow autodiff mechanism.

**Note:** Unfortunately, GPU computations with Tensorflow are not deterministic,
which means that multiple runs may not give the same results, even with the same
starting random seed :(.

## References

More details about receptive fields estimation using ASD prior and empirical
Bayes can be found in the following papers:

- [Sahani, M. & Linden, J. F. Evidence Optimization Techniques for Estimating Stimulus-Response Functions, *Advances in Neural Information Processing Systems 15*, 317-324, 2003](https://papers.nips.cc/paper/2294-evidence-optimization-techniques-for-estimating-stimulus-response-functions)
- [Park M. & Pillow J. W. Receptive Field Inference with Localized Priors. *PLOS Computational Biology 7(10)*, 2011](https://doi.org/10.1371/journal.pcbi.1002219)
- [Aoi, M. C. & Pillow, J. W. Scalable Bayesian inference for high-dimensional neural receptive fields, *bioRxiv*, 2017](https://doi.org/10.1101/212217)
